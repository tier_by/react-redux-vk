let path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: [
        'babel-polyfill',
        'react-hot-loader/patch',
        './src/index'
    ],
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    module: {
        rules: [
            {enforce: 'pre', test: /\.js$/, exclude: /node_modules/, loader: 'eslint-loader'},
            {test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'}
        ]
    }
};